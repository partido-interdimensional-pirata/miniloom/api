# frozen_string_literal: true

json.call(barca, :id, :created_at, :nombre, :descripcion)

json.abordada @current_pirata.barcas.pluck(:id).include?(barca.id)
json.eliminable barca.consensos.count.zero?
json.tripulacion barca.piratas.count
