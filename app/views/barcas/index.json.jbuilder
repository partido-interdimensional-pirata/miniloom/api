# frozen_string_literal: true

json.barcas @barcas, partial: 'barcas/barca', as: :barca
