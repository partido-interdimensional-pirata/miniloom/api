# frozen_string_literal: true

json.consensos @consensos, partial: 'consensos/consenso', as: :consenso,
                           locals: { ultimas: true }
