# frozen_string_literal: true

json.call(posicion, :id, :created_at, :estado, :comentario)

json.pirata posicion.pirata, partial: 'piratas/pirata', as: :pirata
