# frozen_string_literal: true

json.call(pirata, :id, :nick, :email, :telegram_url)
