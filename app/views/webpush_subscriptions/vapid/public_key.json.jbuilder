# frozen_string_literal: true

json.vapid do
  json.public_key Rails.application.credentials.vapid[:public_key]
end
