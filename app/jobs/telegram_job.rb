# frozen_string_literal: true

# Envía las notificaciones por Telegram
class TelegramJob < ApplicationJob
  queue_as :default

  # @param :piratas [Symbol,Array] :all para traer todas, array de IDs para filtrar
  # @param :payload [String] La notificación a enviar en JSON
  def perform(piratas:, payload:)
    # Encontrar todas las piratas con Telegram
    piratas = find_piratas(piratas)
    payload = JSON.parse payload

    piratas.find_each do |pirata|
      Telegram.bots[:default].send_message chat_id: pirata.telegram_user,
                                           parse_mode: 'HTML',
                                           text: "<b>#{payload['subject']}</b>\n\n#{payload['message']}"
    end
  end

  private

  def find_piratas(ids)
    piratas = Pirata.where.not(telegram_user: nil)

    if ids == 'all'
      piratas
    else
      piratas.where(id: ids)
    end
  end
end
