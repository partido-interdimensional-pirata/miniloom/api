# frozen_string_literal: true

# API de Barcas
class BarcasController < ApplicationController
  before_action :authenticate!
  before_action :find_barca, only: %i[abordar abandonar]

  # GET /barcas
  #
  # Devuelve todas las barcas abordadas
  #
  # @return Array [ barcas ]
  def index
    @barcas = current_pirata.barcas
  end

  # GET /barcas/todas
  #
  # Ver todas las barcas
  def todas
    @barcas = Barca.all
  end

  # GET /barcas/:id
  #
  # Ver los detalles de una barca
  #
  # @param :id [Integer] El ID de la barca
  # @return Hash { id: @int, created_at: @date, updated_at: @date,
  #                nombre: @string, descripcion: @text, abordada: @bool }
  def show
    @barca = Barca.find(params[:id])
    return if @barca

    render json: {}, status: :not_found
  end

  # POST /barcas
  #
  # Crear una barca
  #
  # @param barca [Hash] { barca: { nombre: @string, descripcion: @text } }
  # @return Hash { id: @int, created_at: @date, updated_at: @date,
  #                nombre: @string, descripcion: @text, abordada: @bool }
  def create
    @barca = Barca.new(barca_params)
    # La pirata se une a la barca que crea
    @barca.piratas << current_pirata

    if @barca.save
      notify(subject: :create)

      render status: :created
    else
      render json: { errors: @barca.errors.messages },
             status: :unprocessable_entity
    end
  end

  # PUT /barcas/:id
  #
  # Actualizar una barca
  #
  # @param barca [Hash] { barca: { nombre: @string, descripcion: @text } }
  # @return hash { id: @int, created_at: @date, updated_at: @date,
  #                nombre: @string, descripcion: @text, abordada: @bool }
  def update
    # Las piratas solo pueden modificar sus propias barcas
    @barca = current_pirata.barcas.find(params[:id])

    if @barca.update_attributes barca_params
      notify(subject: :update, urgency: :high)
      render status: :no_content
    else
      render json: { errors: @barca.errors.messages },
             status: :unprocessable_entity
    end
  end

  # DELETE /barcas/:id
  #
  # Elimina una barca y su tripulación, solo si no tiene consensos
  #
  # @return hash { id: @int, created_at: @date, updated_at: @date,
  #                nombre: @string, descripcion: @text, abordada: @bool }
  def destroy
    @barca = current_pirata.barcas.find(params[:id])

    if @barca.consensos.count.zero? && @barca.destroy
      notify(subject: :destroy, urgency: :high)
      render status: :no_content
    else
      render json: { errors: @barca.errors.messages },
             status: :unprocessable_entity
    end
  end

  # PUT /barcas/:barca_id/abordar
  #
  # La pirata actual aborda la barca
  def abordar
    @barca.piratas << current_pirata

    if @barca.save
      notify(subject: :abordar, urgency: :'very-low', ttl: 1.day)
      render status: :no_content
    else
      render json: { errors: @barca.errors.messages },
             status: :unprocessable_entity
    end
  end

  # DELETE /barcas/:barca_id/abandonar
  #
  # La pirata actual abandona la barca!
  def abandonar
    if @barca.tripulaciones.find_by!(pirata: current_pirata).destroy
      notify(subject: :abandonar, urgency: :'very-low', ttl: 1.day)
      render status: :no_content
    else
      render status: :unprocessable_entity,
             json: { errors: @barca.errors.messages }
    end
  rescue ActiveRecord::RecordNotFound
    render status: :unprocessable_entity,
           json: { errors: [I18n.t('barcas.abandonar.no_tripulada')] }
  end

  private

  def barca_params
    params.require(:barca).permit(:nombre, :descripcion)
  end

  def find_barca
    @barca = Barca.find(params[:barca_id])
  end

  def get_subject(view)
    I18n.t("barcas.#{view}.subject", barca: @barca.nombre)
  end

  def get_message(view)
    I18n.t("barcas.#{view}.message", nick: current_pirata.nick)
  end

  def get_endpoint(_)
    barca_path(@barca)
  end
end
