# frozen_string_literal: true

# Recibe suscripciones webpush
class WebpushSubscriptionsController < ApplicationController
  before_action :authenticate!

  # POST /webpush_subscriptions
  #
  # Genera una suscripción Webpush
  #
  # @param Hash {
  #               webpush_subscription: {
  #                 endpoint: @string, auth: @string, p256dh: @string } }
  def create
    @subscription = current_pirata.webpush_subscriptions
                                  .build(subscriptions_params)

    if @subscription.save
      render status: :created
    else
      # El único error que podemos tener es que la subscripción ya esté
      # hecha, con lo que no hace falta que la volvamos a crear.
      render status: :no_content
    end
  end

  # GET /webpush_subscriptions/vapid/public_key
  #
  # Devuelve la llave pública VAPID para suscribirse a Webpush
  #
  # @return Hash { vapid: { public_key: @string } }
  def vapid_public_key
    render 'webpush_subscriptions/vapid/public_key'
  end

  private

  def subscriptions_params
    params.require(:webpush_subscription)
          .permit(:endpoint, :auth, :p256dh)
  end
end
