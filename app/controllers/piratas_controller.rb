# frozen_string_literal: true

# Las piratas son las que activan el Miniloom
class PiratasController < ApplicationController
  # Todo necesita autenticación salvo la creación de cuentas
  before_action :authenticate!, except: %i[create]
  # POST /piratas
  #
  # Registra una cuenta
  #
  # @param pirata [Hash] { pirata: { password: @string, email: @string,
  #                                  nick: @string } }
  # @return [Hash] { id: @int, nick: @string, email: @string }
  def create
    @pirata = Pirata.new(params
      .require(:pirata)
      .permit(:email, :nick, :password))

    return if @pirata.save

    render json: { errors: @pirata.errors.messages },
           status: :unprocessable_entity
  end

  # GET /piratas/yo
  #
  # Obtener el perfil propio
  #
  # @return [Hash] { id: @int, nick: @string, email: @string, telegram_url: @string }
  def yo
    @pirata = current_pirata
  end
end
