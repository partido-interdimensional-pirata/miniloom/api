# frozen_string_literal: true

# API para Consensos, necesita autenticar una pirata vía HTTP Basic Auth
#
# @see PiratasController#create
class ConsensosController < ApplicationController
  # Para cualquier acción necesitamos autenticación
  before_action :authenticate!
  before_action :find_barca!

  # GET /barcas/:barca_id/consensos
  #
  # Podemos ver todos los consensos de una barca
  #
  # @return [Array] [ consensos ]
  def index
    @consensos = @barca.consensos.order(:updated_at, :desc)
  end

  # GET /barcas/:barca_id/consensos/:id
  #
  # Podemos ver uno solo sabiendo su ID
  #
  # @param id [Integer] El ID del consenso
  # @return [Hash] { id: @int, created_at: @date,
  #                  titulo: @string, texto: @string,
  #                  pirata: @pirata,
  #                  posiciones: [] }
  def show
    @consenso = @barca.consensos.find(params[:id])
    return if @consenso

    render json: {}, status: :not_found
  end

  # POST /barcas/:barca_id/consensos
  #
  # Podemos crear uno, enviando un hash con todas las propiedades
  #
  # @param consenso [Hash] { consenso: { titulo: @string, texto: @string } }
  # @return [Hash] { id: @int, created_at: @date,
  #                  titulo: @string, texto: @string,
  #                  pirata: @pirata,
  #                  posiciones: [] }
  def create
    @consenso = current_pirata.consensos.new(consenso_params)
    @consenso.barca = @barca

    if @consenso.save
      notify(subject: :create, urgency: :high)
      render status: :created
    else
      render json: { errors: @consenso.errors.messages },
             status: :unprocessable_entity
    end
  end

  # DELETE /barcas/:barca_id/consensos/:id
  #
  # Eliminar un consenso, solo si no tiene posiciones!
  #
  # @param :id [Integer] El ID del consenso a eliminar
  # @return [Hash] { id: @int, created_at: @date,
  #                  titulo: @string, texto: @string,
  #                  posiciones: [] }
  def destroy
    @consenso = @barca.consensos.find(params[:id])

    if @consenso.posiciones.count.zero? && @consenso.destroy
      notify(subject: :destroy)
      render status: :ok
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  private

  # Encuentra la barca en los parámetros
  def find_barca!
    @barca = Barca.find(params[:barca_id])
  end

  # Parámetros permitidos
  def consenso_params
    params.require(:consenso).permit(:titulo, :texto)
  end

  def get_subject(view)
    I18n.t("consensos.#{view}.subject",
           barca: @barca.nombre,
           consenso: @consenso.titulo)
  end

  def get_message(view)
    I18n.t("consensos.#{view}.message",
           consenso: @consenso.texto[0..140],
           nick: current_pirata.nick)
  end

  def get_endpoint(_)
    barca_consenso_path(@barca, @consenso)
  end
end
