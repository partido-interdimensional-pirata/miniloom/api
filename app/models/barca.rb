# frozen_string_literal: true

# Las barcas agrupan consensos y piratas alrededor de un tema específico
# o relación de afinidad
class Barca < ApplicationRecord
  # Tiene muchos consensos
  has_many :consensos

  # En realidad debería tener una sola tripulación que traiga muchas
  # piratas, pero rails es así
  has_many :tripulaciones, dependent: :destroy
  has_many :piratas, through: :tripulaciones

  validates_uniqueness_of :nombre
  validates_presence_of :descripcion
end
