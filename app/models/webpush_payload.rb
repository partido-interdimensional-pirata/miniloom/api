# frozen_string_literal: true

# El payload que se envía en la notificación
#
# @param :subject [String] El título de la notificación
# @param :message [String] El cuerpo de la notificación
# @param :endpoint [String] La URL a visitar al abrir la notificación
WebpushPayload = Struct.new(:subject, :message, :endpoint,
                            keyword_init: true) do
  # Convertir el payload a JSON para poder enviarlo
  #
  # @return String
  def to_json(opts = nil)
    to_h.to_json(opts)
  end

  def self.from_json(string)
    new(JSON.parse(string))
  end
end
