# frozen_string_literal: true

# Gestiona las suscripciones por webpush
class WebpushSubscription < ApplicationRecord
  belongs_to :pirata
  validates :endpoint, url: true
  validates_uniqueness_of :auth, scope: %i[p256dh endpoint],
                                 case_sensitive: false

  URGENCY = %w[very-low low normal high].freeze

  # Envía la notificación
  def payload_send(payload:, urgency: :normal, ttl: 1.day)
    Webpush.payload_send(
      vapid: {
        subject: 'mailto:lumi@partidopirata.com.ar',
        public_key: Rails.application.credentials.vapid[:public_key],
        private_key: Rails.application.credentials.vapid[:private_key]
      },
      endpoint: endpoint, auth: auth, p256dh: p256dh,
      ttl: ttl.to_i, urgency: normalize_urgency(urgency),
      message: payload.to_json
    )
  rescue Webpush::Unauthorized
    Rails.logger.info "#{id} no autorizada"
  end

  private

  def normalize_urgency(urgency)
    if URGENCY.include? urgency.to_s
      urgency.to_s
    else
      'normal'
    end
  end
end
