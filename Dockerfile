FROM alpine:3.10 AS build
ENV PACKAGER "fauno <fauno@partidopirata.com.ar>"
ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1
ENV SECRET_KEY_BASE solo_es_necesaria_para_correr_rake
ENV RAILS_ENV production

# Instalar las herramientas para instalar las gemas y paquetes
RUN apk add --no-cache git tzdata libxslt libxslt-dev libxml2 libxml2-dev alpine-sdk
RUN apk add --no-cache ruby-dev ruby-json ruby-bigdecimal ruby-rake
RUN apk add --no-cache sqlite sqlite-dev
# Crear una usuaria de trabajo, más que nada para que bundler no se
# queje que estamos corriendo como root
RUN addgroup -g 82 -S www-data
RUN adduser -s /bin/sh -G www-data -h /home/app -D app
RUN install -dm 2750 -o app -g www-data /home/app/web
RUN gem install --no-user-install --no-ri --no-rdoc bundler

# find | xargs es más rápido que un chown recursivo
USER app
WORKDIR /home/app/web
COPY --chown=app:www-data ./Gemfile .
COPY --chown=app:www-data ./Gemfile.lock .
# Instalar las gemas de producción
RUN bundle install --path=./vendor --without test development
# Eliminar cosas que ya no sirven
RUN rm vendor/ruby/2.5.0/cache/*.gem
# Limpiar las librerías nativas
RUN find vendor -name "*.so" | xargs -rn 1 strip --strip-unneeded
COPY --chown=app:www-data ./.git/ ./.git/
# Hacer un tarball de los archivos de la web
RUN git archive -o ../web.tar.gz HEAD

FROM lainventoria/monit:3.10
RUN apk add --no-cache libxslt libxml2 tzdata ruby ruby-json ruby-bigdecimal ruby-rake ruby-etc
RUN apk add --no-cache sqlite-libs
RUN apk add --no-cache darkhttpd
RUN gem install --no-user-install --no-ri --no-rdoc bundler
COPY ./entrypoint.sh /usr/bin/entrypoint
RUN chmod 755 /usr/bin/entrypoint
# Agregar el usuario
RUN adduser -s /bin/sh -G www-data -h /srv/http -D app
# Traer los archivos de la web y colocarlos donde van definitivamente
USER app
COPY --from=build --chown=app:www-data /home/app/web.tar.gz /tmp/
WORKDIR /srv/http
RUN tar xf /tmp/web.tar.gz && rm /tmp/web.tar.gz
# Traer las gemas
COPY --from=build --chown=app:www-data /home/app/web/vendor ./vendor
COPY --from=build --chown=app:www-data /home/app/web/.bundle ./.bundle
COPY --chown=app:www-data ./docs ./public/docs

USER root
ARG MASTER_KEY
ENV RAILS_MASTER_KEY=$MASTER_KEY
ENV RAILS_ENV production
RUN install -m 640 -o root -g root ./monit.conf /etc/monit.d/puma.conf
RUN monit -t
RUN install -dm 2700 -o app -g www-data /srv/http/data
VOLUME "/srv/http/data"
EXPOSE 3000
EXPOSE 8080
