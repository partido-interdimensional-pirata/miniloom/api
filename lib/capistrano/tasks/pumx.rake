# frozen_string_literal: true

namespace :pumx do
  desc 'Start puma'
  task :start do
    on roles(:web) do
      within current_path do
        execute :bundle, :exec, :puma, '--daemon'
      end
    end
  end

  desc 'Stop puma'
  task :stop do
    on roles(:web) do
      within current_path do
        pid = "#{current_path}/tmp/pids/puma.pid"
        execute "test -f '#{pid}' && cat '#{pid}' | xargs kill ; :"
      end
    end
  end
end
