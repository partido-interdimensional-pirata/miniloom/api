.DEFAULT_GOAL := all

d ?= api
c ?= $(shell date +%F | tr "-" ".")
r ?= $(shell git rev-list --count HEAD)
name = miniloom/$(d):$(c).$(r)

master_key := $(shell cat config/master.key)

doc:
	./bin/yardoc --output-dir=./docs/ \
		app/controllers/*.rb \
		app/models/*.rb
	chmod -R o=g docs/
	rsync -av --delete-after docs/ lumi@partidopirata.com.ar:/srv/http/api.lumi.partidopirata.com.ar/deploy/shared/docs/

clean:
	rm -f log/*.log
	rm -rf tmp/cache/assets

build:
	docker build --build-arg MASTER_KEY=$(master_key) -t $(name) .

tag:
	git tag $(c).$(r)
	docker tag $(name) miniloom/$(d):latest
	docker tag $(name) registry.forja.lainventoria.com.ar/$(name)
	docker tag $(name) registry.forja.lainventoria.com.ar/miniloom/$(d):latest

push:
	docker push registry.forja.lainventoria.com.ar/$(name)
	docker push registry.forja.lainventoria.com.ar/miniloom/$(d):latest

run:
	docker run -v $(PWD)/data:/srv/http/data \
		-e RAILS_MASTER_KEY=$(master_key) $(name)

all: clean doc build tag
