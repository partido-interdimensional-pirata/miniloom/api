# frozen_string_literal: true

lock '~> 3.16.0'

set :application, 'lumi'
set :repo_url, 'https://0xacab.org/pip/miniloom/api.git'
set :deploy_to, '/srv/http/api.lumi.partidopirata.com.ar/deploy'
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets',
       'public/system', 'data', '.bundle', 'docs'
set :bundle_flags, '--deployment'
set :bundle_jobs, 1
set :keep_releases, 2
set :ssh_options, check_host_ip: true

set :default_env,
    rails_env: 'production',
    rails_master_key: ENV.fetch('RAILS_MASTER_KEY') { File.read('config/master.key') },
    web_concurrency: '1',
    ld_preload: '/usr/lib/libjemalloc.so',
    api_host: 'api.lumi.partidopirata.com.ar'

after 'deploy:published', 'pumx:stop'
after 'pumx:stop', 'pumx:start'
