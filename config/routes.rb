# frozen_string_literal: true

Rails.application.routes.draw do
  telegram_webhook Telegram::WebhookController

  get '/piratas/yo', to: 'piratas#yo'
  # No queremos un índice de piratas
  resources :piratas, only: %i[create]
  # Obtener la llave VAPID para suscripciones
  get '/webpush_subscriptions/vapid/public_key',
      to: 'webpush_subscriptions#vapid_public_key'
  # Solo se pueden crear suscripciones
  resources :webpush_subscriptions, only: %i[create]
  # Podemos crear barcas y dentro de ellas consensos
  get 'barcas/todas', to: 'barcas#todas'
  resources :barcas, only: %i[index show create update destroy] do
    put 'abordar', to: 'barcas#abordar'
    delete 'abandonar', to: 'barcas#abandonar'
    # Podemos crear consensos pero no modificarlos
    resources :consensos, only: %i[index show create destroy] do
      # Y solo le podemos agregar posiciones
      resources :posiciones, only: %i[create]
    end
  end
end
