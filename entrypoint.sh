#!/bin/sh
set -e
export RAILS_ENV=production

case $1 in
  api)
    cd /srv/http
    bundle exec rake db:migrate
    bundle exec rake db:seed
    bundle exec puma -d config.ru
    exit $?
    ;;
  darkhttpd)
    darkhttpd /srv/http/public \
      --no-server-id \
      --pidfile /tmp/darkhttpd.pid \
      --uid darkhttpd \
      --gid www-data \
      --no-listing \
      --daemon \
      --port 8080
    exit $?
    ;;
esac
