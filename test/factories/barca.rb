# frozen_string_literal: true

FactoryBot.define do
  factory :barca do
    nombre { SecureRandom.hex }
    descripcion { SecureRandom.hex }

    transient do
      tripulacion { 0 }
    end

    after :create do |barca, evaluator|
      create_list(:tripulacion, evaluator.tripulacion, barca: barca)
    end
  end
end
