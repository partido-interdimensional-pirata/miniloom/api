# frozen_string_literal: true

FactoryBot.define do
  factory :consenso do
    barca
    pirata
    titulo { 'Estamos a favor de la despenalización del aborto' }
    texto { '...' }

    transient do
      con_posiciones { 0 }
    end

    after :create do |consenso, evaluator|
      create_list(:posicion, evaluator.con_posiciones, consenso: consenso)
    end
  end
end
