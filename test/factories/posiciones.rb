# frozen_string_literal: true

FactoryBot.define do
  factory :posicion do
    pirata
    consenso

    estado { Posicion::ESTADOS.sample }
  end
end
