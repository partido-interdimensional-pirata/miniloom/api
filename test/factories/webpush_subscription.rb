# frozen_string_literal: true

FactoryBot.define do
  factory :webpush_subscription do
    pirata
    auth { SecureRandom.hex }
    p256dh { SecureRandom.hex }
    endpoint { "https://#{SecureRandom.hex}.org/endpoint" }
  end
end
