# frozen_string_literal: true

require 'test_helper'

class PirataTest < ActiveSupport::TestCase
  test 'se pueden crear' do
    pirata = create :pirata

    assert_equal true, pirata.valid?
  end

  test 'tiene un token de telegram' do
    pirate = create :pirata

    assert pirate.telegram_token
  end
end
