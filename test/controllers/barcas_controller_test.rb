# frozen_string_literal: true

class BarcasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pirata = create :pirata
    @auth = { Authorization: ActionController::HttpAuthentication::Basic
            .encode_credentials(@pirata.email, @pirata.password) }
  end

  test 'se pueden ver todas-todas' do
    2.times do
      create :barca
    end

    create :barca, piratas: [@pirata]

    get barcas_todas_url, as: :json, headers: @auth

    body = JSON.parse(response.body)

    assert_equal 200, response.status
    assert_equal 3, body['barcas'].size
  end

  test 'se pueden ver todas' do
    2.times do
      create :barca, piratas: [@pirata]
    end

    create :barca

    get barcas_url, as: :json, headers: @auth

    body = JSON.parse(response.body)

    assert_equal 200, response.status
    assert_equal 2, body['barcas'].size
  end

  test 'se pueden crear' do
    nombre = SecureRandom.hex
    post barcas_url, as: :json, headers: @auth,
                     params: { barca: { nombre: nombre, descripcion: nombre } }

    body = JSON.parse(response.body)

    assert_equal 201, response.status
    assert_equal nombre, body['nombre']
    assert_equal nombre, body['descripcion']
  end

  test 'se pueden editar' do
    barca = create :barca, piratas: [@pirata]
    nombre = SecureRandom.hex

    put barca_url(barca),
        as: :json, headers: @auth,
        params: {
          barca: {
            id: barca.id, nombre: nombre, descripcion: nombre
          }
        }

    assert_equal 204, response.status
  end

  test 'se pueden eliminar' do
    barca = create :barca, piratas: [@pirata]

    delete barca_url(barca), as: :json, headers: @auth

    assert_equal 204, response.status
    assert_raise ActiveRecord::RecordNotFound do
      barca.reload
    end
  end

  test 'no se pueden eliminar si ya tienen consensos' do
    barca = create :barca, piratas: [@pirata]
    barca.consensos << create(:consenso)

    delete barca_url(barca), as: :json, headers: @auth

    assert_equal 422, response.status
    assert barca.reload
  end

  test 'se pueden abandonar' do
    barca = create :barca, piratas: [@pirata]

    delete barca_abandonar_url(barca), as: :json, headers: @auth

    assert_equal 204, response.status
    assert_not barca.reload.piratas.include?(@pirata)
  end

  test 'no se pueden abandonar si no estan abordadas' do
    barca = create :barca

    delete barca_abandonar_url(barca), as: :json, headers: @auth

    body = JSON.parse(response.body)

    assert_response :unprocessable_entity
    assert body['errors'].include?(I18n.t('barcas.abandonar.no_tripulada'))
  end

  test 'se pueden abordar' do
    barca = create :barca

    put barca_abordar_url(barca), as: :json, headers: @auth

    assert_equal 204, response.status
    assert barca.reload.piratas.include?(@pirata)
  end

  test 'hay barcas abordadas' do
    create :barca, piratas: [@pirata]
    create :barca

    get barcas_todas_url, as: :json, headers: @auth

    body = JSON.parse response.body
    assert body['barcas'].first['abordada']
    assert_equal 1, body['barcas'].first['tripulacion']
    assert_not body['barcas'].last['abordada']
    assert_equal 0, body['barcas'].last['tripulacion']
  end
end
