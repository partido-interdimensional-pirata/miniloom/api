# frozen_string_literal: true

# Crear las barcas
class CreateBarcas < ActiveRecord::Migration[5.2]
  def change
    create_table :barcas do |t|
      t.timestamps
      t.string :nombre, unique: true
      t.text :descripcion
    end
  end
end
