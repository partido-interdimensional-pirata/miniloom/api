# frozen_string_literal: true

# Crear la tabla de piratas con la menor cantidad de metadata y d0x
# posible
class CreatePirata < ActiveRecord::Migration[5.2]
  def change
    create_table :pirata do |t|
      t.string :nick, unique: true
      t.string :email, unique: true
      t.string :password_digest
    end
  end
end
