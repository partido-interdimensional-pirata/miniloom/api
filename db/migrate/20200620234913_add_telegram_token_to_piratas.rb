# frozen_string_literal: true

# El token permite asociar piratas con cuentas de Telegram
class AddTelegramTokenToPiratas < ActiveRecord::Migration[5.2]
  def up
    add_column :piratas, :telegram_token, :string, index: true
    add_column :piratas, :telegram_user, :string, index: true

    Pirata.find_each do |pirata|
      pirata.telegram_token!
      pirata.save
    end
  end

  def down
    remove_column :piratas, :telegram_token
    remove_column :piratas, :telegram_user
  end
end
